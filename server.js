const express = require('express');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('./lib/passport');
const viewRoute = require ("./Route/display");
const loginController = require ("./Route/controller-login");
const editController = require ("./Route/controller-edit-player");
const apiRoutes = require ("./Route/controller-api");
const app = express();
const PORT = 8080;

app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'Toyger_Rapha',
}))

app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

app.set("view engine", "ejs");
app.set('views', __dirname + '/views');

app.use ('',viewRoute,loginController,editController);
app.use ('/api/v1',apiRoutes)

app.listen (PORT, () => {
    console.log("server on 8080");
});
