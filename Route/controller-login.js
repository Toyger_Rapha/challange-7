const express = require('express');
const passport = require('../lib/passport')
const router = express.Router();

router.post('/db-log', passport.authenticate('local', {
    successRedirect:'/list-player',
    failureRedirect:'/login',
    failureFlash:true
}));

module.exports = router;