const express = require('express');
const router = express.Router();
const passportJWT = require ('../lib/passport-jwt');
const process = require ('../Controller/Control-api/Process')
router.use(express.json());

const janganJWT = passportJWT.authenticate('jwt', {
        session: false
})

router.post('/register',process.regist);
router.post('/login',process.lgi);
router.post('/create-room',janganJWT,process.createR);
router.post('/fight/:roomId', janganJWT, process.fight)
router.post('/joint/:roomId', janganJWT, process.joinPlay);
router.get('/me', janganJWT, process.me);
router.post('/winner/:roomId', process.win)

module.exports = router;