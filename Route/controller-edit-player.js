const express = require('express');
const router = express.Router();
const process = require ('../Controller/Control/Process')

router.get("/:id/edit", process.edit);
router.get("/db/:id/delete", process.delete);
router.post("/db/:id/edit", process.editS);
router.post("/db-input/register", process.regist);
router.post("/db-input/add-player", process.addOns);

module.exports = router;