const { user, detail, game_room } = require('../../models');

function format(userx) {
    const { id, username } = userx
    return {
        id,
        username,
        accessToken : userx.generateToken ()
    }
}

const inputScore = (p1Hand, p2Hand) =>{
    if ((p1Hand == "R" && p2Hand == "S") || (p1Hand == "S" && p2Hand == "P") || (p1Hand == "S" && p2Hand == "R")) {
        return 1
    }
    if ((p1Hand == "R" && p2Hand == "R") || (p1Hand == "S" && p2Hand == "S") || (p1Hand == "P" && p2Hand == "P")) {
        return 0
    }
    if ((p1Hand == "S" && p2Hand == "R") || (p1Hand == "P" && p2Hand == "S") || (p1Hand == "R" && p2Hand == "P")) {
        return -1
    }
}

module.exports = {
    regist : (req,res)=>{
        const { username, password, fullname, alias_name, birthplace } = req.body;
        if (username === undefined || username === "" || username ==="  "){
            return res.json({msg:"Silahkan input username"})
        } if (password === undefined || password === "" || password ==="  "){
            return res.json({msg:"Silahkan input password"})
        } if (fullname === undefined || fullname === "" || fullname ==="  "){
            return res.json({msg:"Silahkan input fullname"})
        } if (alias_name === undefined || alias_name === "" || alias_name ==="  "){
            return res.json({msg:"Silahkan input alias_name"})
        } if (birthplace === undefined || birthplace === "" || birthplace ==="  "){
            return res.json({msg:"Silahkan input birthplace"})
        };
    
        user.register({
            username,
            password
        }).then(user =>{
            detail.create({
                id:user.id,
                id_user:user.id,
                fullname,
                alias_name,
                birthplace
            }).then(()=>{
                res.json({msg:"Input Berhasil, Silahkan Login"})
            }) 
        })
    },
    
    lgi : (req,res) =>{
        user.authenticate(req.body)
            .then(userx =>{
                res.json(
                    format(userx)
                )
            })
    },
    
    createR : (req,res)=>{
        const {id} = req.user.dataValues
        game_room.create({
            p1_id:id
        }).then(room=>res.json({
            roomId: room.id
        }))
    },
       
    joinPlay : (req,res) =>{ 
        const {roomId} = req.params
        const { id } = req.user.dataValues

        game_room.findOne({where :{
            id : roomId
        }}).then(room =>{
            if (room.p2_id !=null) {
                return res.json({msg:"Player 2 Sudah Masuk atau Terdaftar"})
            }

            game_room.update({
                p2_id : id
            }, {where : {id : roomId}})
            .then(()=>{
                res.json({msg :"Player 2 Berhasil Bergabung Dalam Game"})
            })
        })
    },

    fight : (req,res)=>{
        const { roomId } = req.params
        const { id } = req.user.dataValues 
        const { firstHand, secondHand, thirdHand }  = req.body
        let isP1 = false
        let isP2 = false

        if (!firstHand || !secondHand || !thirdHand) {
            return res.json({msg:"Silahkan input firstHand, secondHand dan thirdHand"})
        }

        // Buat ngecek data di room
        game_room.findOne({where : { id : roomId}})
        .then(room =>{
            if(room.Winner_id != null){
                return res.json({msg:"Game Sudah Dimainkan"})
            }
            if(room.p1_id == id ){
                isP1 = true
            }
            if(room.p2_id == id){
                isP2 = true
            }
            if( !isP1 && !isP2){
                return res.json({msg:"Penyusup"})
            }
            if(isP1){
                game_room.update({
                    p1_firstHand : firstHand,
                    p1_secondHand : secondHand,
                    p1_thirdHand : thirdHand,
                }, {where : { id : roomId}}).then (() => res.json({msg: "Berhasil memasukan input"}))
            }
            if(isP2){
                game_room.update({
                    p2_firstHand : firstHand,
                    p2_secondHand : secondHand,
                    p2_thirdHand : thirdHand,
                }, {where : { id : roomId}}).then (() => res.json({msg: "Berhasil memasukan input"}))
            }
        })
    },

    win : (req,res)=>{
        const { roomId } = req.params
        game_room.findOne({where : { id : roomId}}).then(room =>{
            let balancer = 0
            balancer = balancer + inputScore(room.p1_firstHand, room.p2_firstHand)
            balancer = balancer + inputScore(room.p1_secondHand, room.p2_secondHand)
            balancer = balancer + inputScore(room.p1_thirdHand, room.p2_thirdHand)
            console.log(balancer)
            if (balancer > 0) {
                game_room.update({
                    Winner_id : p1_id
                }, {where : { id : roomId}}).then (()=> res.json({msg:"Player 1 Menang"}))
            }
            if (balancer == 0) {
                game_room.update({
                    Winner_id : 0
                }, {where : { id : roomId}}).then (()=> res.json({msg:"Draw"}))
            }
            if (balancer < 0) {
                game_room.update({
                    Winner_id : p2_id
                }, {where : { id : roomId}}).then (()=> res.json({msg:"Player 2 Menang"}))
            }

        })
    },   

    me : (req,res)=>{
        const {username} = req.user.dataValues
        res.send('akun aktif ' + username)
    }
}