const { user, detail } = require('../../models');

module.exports = {
    edit : (req,res) =>{
        const { id } = req.params
        user.findOne ({
            where : {id},
            include: detail
        }).then (x=>{
            res.render('edit-player.ejs',{x})
        })
    },
    delete : (req,res)=>{
        const { id } = req.params
        user.destroy({
            where : {id}
        }).then (()=>{
            detail.destroy({
                where: {id}
            }).then (()=>{
                res.redirect("/list-player")
            })
        })
    },
    editS : (req,res)=>{
        const {id} = req.params
        const { username, password, fullname, alias_name, birthplace } = req.body
        user.update({
            username,
            password,
        },{where : {id}
        }).then (()=>{
            detail.update({
                fullname,
                alias_name,
                birthplace,
            },{where : {id}
        }).then(()=>{
            res.redirect("/list-player")
        })
        })
    }, 
    regist : (req,res)=>{
        const { username, password, fullname, alias_name, birthplace } = req.body
        user.register({
            username,
            password
        }).then(user =>{
            detail.create({
                id:user.id,
                id_user:user.id,
                fullname,
                alias_name,
                birthplace
            }).then(()=>{
                res.redirect("/login")
            })
        })
    },
    addOns : (req,res)=>{
        const { username, password, fullname, alias_name, birthplace } = req.body 
        user.register({
            username,
            password
        }).then(user =>{
            detail.create({
                id:user.id,
                id_user:user.id,
                fullname,
                alias_name,
                birthplace,
            }).then(()=>{
                res.redirect("/list-player")
            })
        })
    },
    
}